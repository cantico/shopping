## Boutique en ligne ##

## Rewriting ##

Apache configuration file to rewrite shop url:


```htaccess

<IfModule mod_rewrite.c>

    Options +FollowSymLinks
    RewriteEngine on
    RewriteBase /

    RewriteRule ^Shop/([^/]+)$ index.php?tg=addon/catalog/main&idx=catalogItem:rewritearticle&title=$1 [L,QSA]

    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    RewriteCond %{REQUEST_FILENAME} !-l

    
    RewriteRule ^(.*)$ index.php?babrw=$1 [L,QSA]

</IfModule>

```