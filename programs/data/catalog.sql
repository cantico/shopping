INSERT INTO shop_catalog (id, name, parentcatalog, catalog_displaymode, catalogitem_displaymode, sortkey) VALUES 

('1', 'Vins de Bordeaux'		, '0', '1', '1', '1'),
('2', 'Vins d''autres Régions'		, '0', '1', '1', '14'),
('3', 'Champagne'			, '0', '1', '1', '22'),
('4', 'Accessoires du vin'		, '0', '1', '1', '23'),
('5', 'Coffrets cadeaux'		, '0', '1', '1', '24'),


('6', 'Vins de Bourgogne'		, '2', '1', '1', '15'),
('7', 'Vins du Languedoc Rousillon'	, '2', '1', '1', '16'),
('8', 'Vins de Loire'			, '2', '1', '1', '17'),
('9', 'Vins de Provence'		, '2', '1', '1', '18'),
('10', 'Vallée du Rhône'		, '2', '1', '1', '19'),
('11', 'Beaujolais'			, '2', '1', '1', '20'),
('12', 'Alsace'				, '2', '1', '1', '21'),


('13', 'Vins Rouges'			, '1', '1', '1', '2'),
('14', 'Vins Blancs'			, '1', '1', '1', '12'),
('15', 'Vins Rosés'			, '1', '1', '1', '13'),

('16', 'Bordeaux et Bordeaux Supérieurs', '13', '1', '1', '3'),
('17', 'Les Vins de Côtes'		, '13', '1', '1', '4'),
('18', 'Fronsac et Canon Fronsac'	, '13', '1', '1', '5'),
('19', 'Satellites de Saint Emilion'	, '13', '1', '1', '6'),
('20', 'Saint Emilion'			, '13', '1', '1', '7'),
('21', 'Pomerol  et Lalande de Pomerol'	, '13', '1', '1', '8'),
('22', 'Graves et Pessac léognan'	, '13', '1', '1', '9'),
('23', 'Médoc'				, '13', '1', '1', '10'),
('24', 'Appellations communales du Médoc', '13', '1', '1', '11')

;
