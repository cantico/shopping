<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */





function shopping_upgrade($version_base, $version_ini)
{
	ini_set('memory_limit', 64 * 1024 * 1024);
	global $babDB;

	require_once dirname(__FILE__) . '/functions.php';
	require_once dirname(__FILE__) . '/crm.php';

	$babDB->db_query('SET FOREIGN_KEY_CHECKS = 0');

	$addon = bab_getAddonInfosInstance('shopping');
	$addon->registerFunctionality('Crm/Shopping', 'crm.php');

	$Crm = shop_Crm();
	$tables = $synchronize = $Crm->synchronizeSql('shop_');


	// bab_installWindow::message(print_r($tables->getDifferences(), true));

	if ($synchronize->isEmpty('shop_country')) {
		if ($Crm->CountrySet()->populateFromGeoNames()) {
			bab_installWindow::message(bab_toHtml(shop_translate('Countries database initialization... done')));
		}
	}

	/*
	if ($synchronize->isEmpty('shop_catalog')) {
		if (bab_execSqlFile(dirname(__FILE__).'/data/catalog.sql', bab_Charset::UTF_8)) {
			bab_installWindow::message(bab_toHtml(shop_translate('Categories initialization... done')));
		}
	}
	*/

	if ($synchronize->isEmpty('shop_articletype')) {
		if (bab_execSqlFile(dirname(__FILE__).'/data/articletype.sql', bab_Charset::UTF_8)) {
			bab_installWindow::message(bab_toHtml(shop_translate('Article types initialization... done')));
		}
	}

	if ($synchronize->isEmpty('shop_packaging')) {
		if (bab_execSqlFile(dirname(__FILE__).'/data/packaging.sql', bab_Charset::UTF_8)) {
			bab_installWindow::message(bab_toHtml(shop_translate('Packaging unit initialization... done')));
		}
	}

	if ($synchronize->isEmpty('shop_articleavailability')) {
		if (bab_execSqlFile(dirname(__FILE__).'/data/articleavailability.sql', bab_Charset::UTF_8)) {
			bab_installWindow::message(bab_toHtml(shop_translate('Article availability initialization... done')));
		}
	}
	
	if ($synchronize->isEmpty('shop_vat')) {
		if (bab_execSqlFile(dirname(__FILE__).'/data/vat.sql', bab_Charset::UTF_8)) {
			bab_installWindow::message(bab_toHtml(shop_translate('VAT rates initialization... done')));
		}
	}


	

	include_once $GLOBALS['babInstallPath']."admin/acl.php";
	aclCreateTable('shop_access_groups');

	if ('' === aclGetRightsString('shop_access_groups', 1)) {

		// set administrators as default group for staff
		aclAdd('shop_access_groups', BAB_ADMINISTRATOR_GROUP, 1);
	}
	
	
	
	$addon->addEventListener('bab_eventBeforeSiteMapCreated', 'shop_onSiteMapItems', 'events.php');
	$addon->addEventListener('bab_eventPageRefreshed', 'shop_onPageRefreshed', 'events.php');

	$Crm->Event()->onUpgrade('shopping');

	
	// force icons_oxygen
	
	require_once $GLOBALS['babInstallPath'] . 'utilit/functionalityincl.php';
	$func = new bab_functionalities();
	$func->copyToParent('Icons/Oxygen');
	
	return true;
}



function shopping_onDeleteAddon()
{

	$Crm = @bab_Functionality::get('Crm/shopping');
	if ($Crm)
	{
		$Crm->Event()->onDelete('shopping');
	}
	
	$addon = bab_getAddonInfosInstance('shopping');
	$addon->unregisterFunctionality('Crm/shopping');
	
	$addon->removeAllEventListeners();

	return true;
}


