<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */




/**
 * Sitemap creation
 * @param bab_eventBeforeSiteMapCreated $event
 * @return mixed
 */
function shop_onSiteMapItems(bab_eventBeforeSiteMapCreated $event)
{

	require_once dirname(__FILE__).'/functions.php';

	bab_functionality::includefile('Icons');


	if (bab_isAccessValid('shop_access_groups', 1)) {
		$link = $event->createItem('shoppingAdmin');
		$link->setLabel(shop_translate('Online shop'));
		$link->setLink('?tg=addon/shopping/main&idx=shopAdmin.home');
		$link->setPosition(array('root', 'DGAll', 'babUser', 'babUserSectionAddons'));

		$event->addFunction($link);
	}



	if (bab_isUserAdministrator()) {
		$link = $event->createItem('shoppingAdminConfig');
		$link->setLabel(shop_translate('Online shop'));
		$link->setLink('?tg=addon/shopping/main&idx=shopAdmin.setAdminGroups');
		$link->setPosition(array('root', 'DGAll', 'babAdmin', 'babAdminSectionAddons'));
		$link->addIconClassname(Func_Icons::ACTIONS_SET_ACCESS_RIGHTS);

		$event->addFunction($link);
	}

}



function shop_onPageRefreshed(bab_eventPageRefreshed $event)
{
    
    $babBody = bab_getBody();
    $addon = bab_getAddonInfosInstance('shopping');
    $babBody->addStyleSheet($addon->getStylePath().'main.css');
}
