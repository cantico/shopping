<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */

$addon = bab_getAddonInfosInstance('shopping');


define('FUNC_SHOPPING_PHP_PATH', $addon->getPhpPath());
define('FUNC_SHOPPING_SET_PATH', FUNC_SHOPPING_PHP_PATH. 'set/');
define('FUNC_SHOPPING_UI_PATH', FUNC_SHOPPING_PHP_PATH . 'ui/');
define('FUNC_SHOPPING_CTRL_PATH', FUNC_SHOPPING_PHP_PATH . 'ctrl/');
define('FUNC_SHOPPING_NOTIFY_PATH', FUNC_SHOPPING_PHP_PATH . 'notify/');


define('SHOPPING_NL_NEWSLETTER', 253021);
define('SHOPPING_NL_FLASHSALES', 253022);



bab_functionality::includeFile('Crm');




class Func_Crm_Shopping extends Func_Crm
{
	
	public $loginByEmail = true;
	
	public $onlineShop = true;
	
	/**
	 * Rewriting default=false
	 */
	//public $rewriting = true;
	

	public function __construct()
	{
		parent::__construct();

		$this->addonPrefix = 'shop';
		$this->classPrefix = $this->addonPrefix . '_';
		$this->controllerTg = 'addon/shopping/main';
		
		
	}

	/**
	 * @return string
	 * 
	 */
	public function getDescription()
	{
		return 'Provides a library of CRM Shop objects.';
	}



	public function includeArticleLinkSet()
	{
		parent::includeArticleLinkSet();
		parent::createEmptySet('ArticleLink');
	}

	
	/**
	 * Includes ShoppingCartSet class definition.
	 */
	public function includeShoppingCartSet()
	{
		parent::includeShoppingCartSet();
		parent::createEmptySet('ShoppingCart');
	}
	
	/**
	 * Includes ShoppingCartItemSet class definition.
	 */
	public function includeShoppingCartItemSet()
	{
		parent::includeShoppingCartItemSet();
		parent::createEmptySet('ShoppingCartItem');
	}
	
	
	/**
	 * Includes ShippingScaleSet class definition.
	 */
	public function includeShippingScaleSet()
	{
		parent::includeShippingScaleSet();
		parent::createEmptySet('ShippingScale');
	}
	

	/**
	 * Includes ArticleSet class definition.
	 */
	public function includeArticleSet()
	{
		parent::includeArticleSet();
		parent::createEmptySet('Article');
	}
	
	
    public function includePrivateSellSet()
    {
    	parent::includePrivateSellSet();
    	parent::createEmptySet('PrivateSell');
    }
    
	
    public function includeArticlePrivateSellSet()
    {
    	parent::includeArticlePrivateSellSet();
    	parent::createEmptySet('ArticlePrivateSell');
    }
	
	
	public function includeArticleTypeSet()
	{
		parent::includeArticleTypeSet();
		parent::createEmptySet('ArticleType');
	}
	
	
	public function includeArticleAvailabilitySet()
	{
		parent::includeArticleAvailabilitySet();
		parent::createEmptySet('ArticleAvailability');
	}
	
	
	public function includeArticlePackagingSet()
	{
		parent::includeArticlePackagingSet();
		parent::createEmptySet('ArticlePackaging');
	}
	
	
	public function includePackagingSet()
	{
		parent::includePackagingSet();
		parent::createEmptySet('Packaging');
	}
	
	public function includeCustomFieldSet()
	{
		parent::includeCustomFieldSet();
		parent::createEmptySet('CustomField');
	}
	
	
	/**
	 * Includes CatalogSet class definition.
	 */
	public function includeCatalogSet()
	{
		parent::includeCatalogSet();
		parent::createEmptySet('Catalog');
	}
	
	
	
	/**
	 * Includes CatalogItemSet class definition.
	 */
	public function includeCatalogItemSet()
	{
		parent::includeCatalogItemSet();
		parent::createEmptySet('CatalogItem');
	}
	
	
	
	/**
	 * Includes CouponSet class definition.
	 */
	public function includeCouponSet()
	{
		parent::includeCouponSet();
		parent::createEmptySet('Coupon');
	}
	
	
	
	/**
	 * Includes CouponUsageSet class definition.
	 */
	public function includeCouponUsageSet()
	{
		parent::includeCouponUsageSet();
		parent::createEmptySet('CouponUsage');
	}
	
	
	/**
	 * Includes PaymentErrorSet class definition.
	 */
	public function includePaymentErrorSet()
	{
		parent::includePaymentErrorSet();
		parent::createEmptySet('PaymentError');
	}
	


	/**
	 * Includes ContactSet class definition.
	 */
	public function includeContactSet()
	{
		parent::includeContactSet();
		parent::createEmptySet('Contact');
	}




	/**
	 * Includes AddressSet class definition.
	 */
	public function includeAddressSet()
	{
		parent::includeAddressSet();
		parent::createEmptySet('Address');
	}


	/**
	 * Includes CountrySet class definition.
	 */
	public function includeCountrySet()
	{
		parent::includeCountrySet();
		parent::createEmptySet('Country');
	}




	/**
	 * Includes NoteSet class definition.
	 */
	public function includeNoteSet()
	{
		parent::includeNoteSet();
		parent::createEmptySet('Note');
	}
	
	
	/**
	 * Includes CommentSet class definition.
	 */
	public function includeCommentSet()
	{
		parent::includeCommentSet();
		parent::createEmptySet('Comment');
	}



	/**
	 * Includes EmailSet class definition.
	 */
	public function includeEmailSet()
	{
		parent::includeEmailSet();
		parent::createEmptySet('Email');
	}
	
	
	
	/**
	 * Includes OrderSet class definition.
	 */
	public function includeOrderSet()
	{
		parent::includeOrderSet();
		parent::createEmptySet('Order');
	}
	
	
	/**
	 * Includes OrderItemSet class definition.
	 */
	public function includeOrderItemSet()
	{
		parent::includeOrderItemSet();
		parent::createEmptySet('OrderItem');
	}


	/**
	 * Includes OrderTaxSet class definition.
	 */
	public function includeOrderTaxSet()
	{
		parent::includeOrderTaxSet();
		parent::createEmptySet('OrderTax');
	}
	
	
	
	/**
	 * Includes PasswordTokenSet.
	 */
	public function includePasswordTokenSet()
	{
		parent::includePasswordTokenSet();
		parent::createEmptySet('PasswordToken');
	}

	
	/**
	 * Includes SearchHistorySet.
	 */
	public function includeSearchHistorySet()
	{
		parent::includeSearchHistorySet();
		parent::createEmptySet('SearchHistory');
	}
	
	
	

	public function includeVatSet()
	{
		parent::includeVatSet();
		parent::createEmptySet('Vat');
	}
	
	
	public function includeShopTraceSet()
	{
		parent::includeShopTraceSet();
		parent::createEmptySet('ShopTrace');
	}

	
	public function includeImportSet()
	{
	    parent::includeImportSet();
	    parent::createEmptySet('Import');
	}
	
	
	protected function includeAccess()
	{
	    parent::includeAccess();
	    require_once FUNC_SHOPPING_PHP_PATH . 'access.class.php';
	}


	/**
	 * Includes Controller class definition.
	 */
	public function includeController()
	{
		parent::includeController();
		require_once FUNC_SHOPPING_PHP_PATH . 'controller.class.php';
	}


	public function includeUi()
	{
	    parent::includeUi();
	    require_once FUNC_SHOPPING_UI_PATH . 'ui.class.php';
	}
}

