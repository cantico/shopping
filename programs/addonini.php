; <?php/*

[general]
name				="shopping"
version				="0.1.2"
addon_type			="EXTENSION"
addon_access_control=0
encoding			="UTF-8"
mysql_character_set_database="latin1,utf8"
description			="Online shop"
description.fr		="Module de boutique en ligne"
package_creation	="LibOrm, LibCrm, jquery, widgets, theme_shopping, sitemap_editor, googleanalytics, LibPayment, LibPaymentSips, LibNewsletter"
delete				=1
db_prefix			="shop_"
ov_version			="8.2.0"
php_version			="5.2.0"
mysql_version		="4.0"
author				="Cantico"
icon				="Retail-Shop.png"
configuration_page	="main&idx=shopadmin:setAdminGroups"
tags                ="extension,shop"


[addons]
widgets				="1.0.18"
LibOrm				="0.8.2"
LibCrm				="1.0.6"
LibFileManagement 	="0.2.31"
captcha			 	="0.5"
LibPayment			="0.0.1"
LibPdf				="0.0.5"
LibGeoNames			="0.0.6"
AuthEmail			="0.1"
theme_shopadmin		="0.0.2"
jquery				="1.7.1.4"
LibTranslate	    ="1.5.2.04"
googleanalytics		="0.1"
LibNewsletter		="1.0.5"
icons_oxygen        ="0.6.4"


[functionalities]
jquery				="Available"
Crm					="Available"
Thumbnailer			="Available"


;*/ ?>