��    	      d      �       �   +   �   $     !   2  )   T     ~  %   �     �      �  4  �  7   %  +   ]  &   �  6   �     �  +   �  #   %  &   I            	                                  Article availability initialization... done Article types initialization... done Categories initialization... done Countries database initialization... done Online shop Packaging unit initialization... done Regions initialization... done VAT rates initialization... done Project-Id-Version: advshop
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-07-23 16:09+0200
PO-Revision-Date: 2015-07-23 16:09+0200
Last-Translator: Paul de Rosanbo <paul.derosanbo@cantico.fr>
Language-Team: Cantico <paul.derosanbo@cantico.fr>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-SourceCharset: iso-8859-1
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: shop_translate;shop_translate:1,2
X-Generator: Poedit 1.8.2
X-Poedit-SearchPath-0: programs
 Initialisation des disponibilités des produits... fait Initialisation des types de produit... fait Initialisation des catégories... fait Initialisation de la base de données des pays... fait Boutique en ligne Initialisation des unités de vente... fait Initialisation des régions... fait Initialisation des taux de TVA... fait 